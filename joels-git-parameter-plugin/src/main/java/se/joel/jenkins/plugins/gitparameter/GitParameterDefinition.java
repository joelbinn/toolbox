package se.joel.jenkins.plugins.gitparameter;

import hudson.EnvVars;
import hudson.Extension;
import hudson.model.AbstractProject;
import hudson.model.Hudson;
import hudson.model.ParameterDefinition;
import hudson.model.ParameterValue;
import hudson.model.ParametersDefinitionProperty;
import hudson.model.TaskListener;
import hudson.plugins.git.GitAPI;
import hudson.plugins.git.GitException;
import hudson.plugins.git.GitSCM;
import hudson.scm.SCM;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.eclipse.jgit.transport.RemoteConfig;
import org.eclipse.jgit.transport.URIish;
import org.kohsuke.stapler.DataBoundConstructor;
import org.kohsuke.stapler.StaplerRequest;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;


public class GitParameterDefinition extends ParameterDefinition implements Comparable<GitParameterDefinition> {
    private static final long serialVersionUID = 9157832967140868122L;

    public static final String PARAMETER_TYPE_TAG = "PT_TAG";
    public static final String PARAMETER_TYPE_REVISION = "PT_REVISION";

    private final UUID uuid;

    @Extension
    public static class DescriptorImpl extends ParameterDescriptor {
        @Override
        public String getDisplayName() {
            return "Joels Git Parameter";
        }
    }

    public static class LogEntry {
        String sha1;
        Long timeStamp;
        String displayString;

        LogEntry(String sha1, String timeStamp, String displayString) {
            this.sha1 = sha1;
            this.timeStamp = Long.parseLong(timeStamp);
            this.displayString = displayString;
        }

        public String getSha1() {
            return sha1;
        }

        public Long getTimeStamp() {
            return timeStamp;
        }

        public String getDisplayString() {
            return displayString;
        }
    }

    private String type;
    private String branch;

    private String errorMessage;
    private String defaultValue;

    private Map<Long, LogEntry> revisionMap;
    private Map<String, String> tagMap;

    @DataBoundConstructor
    public GitParameterDefinition(String name,
            String type, String defaultValue,
            String description, String branch
                                 ) {
        super(name, description);
        this.type = type;
        this.defaultValue = defaultValue;
        this.branch = branch;

        this.uuid = UUID.randomUUID();
    }


    @Override
    public ParameterValue createValue(StaplerRequest request) {
        String value[] = request.getParameterValues(getName());
        if (value == null) {
            return getDefaultParameterValue();
        }
        return null;
    }

    @Override
    public ParameterValue createValue(StaplerRequest request, JSONObject jO) {
        Object value = jO.get("value");
        String strValue = "";
        if (value instanceof String) {
            strValue = (String) value;
        } else if (value instanceof JSONArray) {
            JSONArray jsonValues = (JSONArray) value;
            for (int i = 0; i < jsonValues.size(); i++) {
                strValue += jsonValues.getString(i);
                if (i < jsonValues.size() - 1) {
                    strValue += ",";
                }
            }
        }

        if ("".equals(strValue)) {
            strValue = defaultValue;
        }

        GitParameterValue gitParameterValue = new GitParameterValue(jO.getString("name"), strValue);
        return gitParameterValue;
    }

    @Override
    public ParameterValue getDefaultParameterValue() {
        String defValue = getDefaultValue();
        if (!StringUtils.isBlank(defValue)) {
            return new GitParameterValue(getName(), defValue);
        }
        return super.getDefaultParameterValue();
    }


    @Override
    public String getType() {
        return type;
    }


    public void setType(String type) {
        if (type.equals(PARAMETER_TYPE_TAG) || type.equals(PARAMETER_TYPE_REVISION)) {
            this.type = type;
        } else {
            this.errorMessage = "wrongType";

        }
    }

    public String getBranch() {
        return this.branch;
    }

    public void setBranch(String nameOfBranch) {
        this.branch = nameOfBranch;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }


    public AbstractProject<?, ?> getParentProject() {
        AbstractProject<?, ?> context = null;
        List<AbstractProject> jobs = Hudson.getInstance().getItems(AbstractProject.class);

        for (AbstractProject<?, ?> project : jobs) {
            ParametersDefinitionProperty property =
                    (ParametersDefinitionProperty) project.getProperty(ParametersDefinitionProperty.class);

            if (property != null) {
                List<ParameterDefinition> parameterDefinitions = property.getParameterDefinitions();

                if (parameterDefinitions != null) {
                    for (ParameterDefinition pd : parameterDefinitions) {

                        if (pd instanceof GitParameterDefinition &&
                            ((GitParameterDefinition) pd).compareTo(this) == 0) {

                            context = project;
                            break;
                        }
                    }
                }
            }
        }

        return context;
    }

    @Override
    public int compareTo(GitParameterDefinition pd) {
        if (pd.uuid.equals(uuid)) {
            return 0;
        }

        return -1;
    }


    public void generateContents(String contenttype) {

        AbstractProject<?, ?> project = getParentProject();


        // for (AbstractProject<?,?> project : Hudson.getInstance().getItems(AbstractProject.class)) {
        if (project.getSomeWorkspace() == null) {
            this.errorMessage = "You have to build your project at least once to use this parameter.";
        }

        SCM scm = project.getScm();

        //if (scm instanceof GitSCM); else continue;
        if (!(scm instanceof GitSCM)) {
            this.errorMessage = "Need to have Git as SCM";
        }


        GitSCM git = (GitSCM) scm;

        String defaultGitExe = File.separatorChar != '/' ? "git.exe" : "git";
        GitSCM.DescriptorImpl desc = (GitSCM.DescriptorImpl) git.getDescriptor();
        if (desc.getOldGitExe() != null) {
            defaultGitExe = desc.getOldGitExe();
        }

        EnvVars environment = null;

        try {
            environment = project.getSomeBuildWithWorkspace().getEnvironment(TaskListener.NULL);
        } catch (Exception e) {
        }

        for (RemoteConfig repository : git.getRepositories()) {
            for (URIish remoteURL : repository.getURIs()) {
                GitAPI newgit =
                        new GitAPI(defaultGitExe, project.getSomeWorkspace(), TaskListener.NULL, environment, "");
                newgit.fetch();
                if (type.equalsIgnoreCase(PARAMETER_TYPE_REVISION)) {
                    revisionMap = getLogEntries(newgit);
                } else if (type.equalsIgnoreCase(PARAMETER_TYPE_TAG)) {
                    tagMap = new HashMap<String, String>();
                    //Set<String> tagNameList = newgit.getTagNames("*");
                    for (String tagName : newgit.getTagNames("*")) {
                        tagMap.put(tagName, tagName);
                    }
                }

            }
        }
    }

    public Map<Long, LogEntry> getLogEntries(GitAPI git) throws GitException {
        Map<Long, LogEntry> entries = new TreeMap<Long, LogEntry>(new Comparator<Long>() {
            @Override
            public int compare(Long o1, Long o2) {
                if (o1 > o2) {
                    return -1;
                }
                if (o1 < o2) {
                    return 1;
                }
                return 0;
            }
        });
        String result = git.launchCommand("log", "--pretty=format:%H::>>%ct::>>%ci %cn \"%f\"");
        BufferedReader rdr = new BufferedReader(new StringReader(result));
        String line;
        try {
            while ((line = rdr.readLine()) != null) {
                // Add SHA1
                String[] split = line.split("::>>");
                LogEntry logEntry = new LogEntry(split[0], split[1], split[2]);
                entries.put(logEntry.timeStamp, logEntry);
            }
        } catch (IOException e) {
            throw new GitException("Error parsing rev list", e);
        }

        return entries;
    }


    public String getErrorMessage() {
        return errorMessage;
    }

    public String getError() {
        return "error";
    }

    public Map<Long, LogEntry> getRevisionMap() {
        generateContents(PARAMETER_TYPE_REVISION);
        return revisionMap;
    }

    public Map<String, String> getTagMap() {
        generateContents(PARAMETER_TYPE_TAG);
        return tagMap;
    }


}
